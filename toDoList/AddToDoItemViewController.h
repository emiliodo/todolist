//
//  AddToDoItemViewController.h
//  toDoList
//
//  Created by INFTEL 24 on 12/1/15.
//  Copyright (c) 2015 EmilioDonaire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ToDoItem.h"

@interface AddToDoItemViewController : UIViewController
@property ToDoItem *toDoItem;
@end
