//
//  ToDoItem.h
//  toDoList
//
//  Created by INFTEL 24 on 12/1/15.
//  Copyright (c) 2015 EmilioDonaire. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ToDoItem : NSObject
@property ToDoItem *toDoItem;
@property NSString *itemName;
@property BOOL completed;
@property (readonly) NSDate *creationDate;

@end
